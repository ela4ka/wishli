from django.db import models


class TimeStampModelMixin(models.Model):
    created_at = models.DateTimeField('дата добавления', auto_now_add=True)
    updated_at = models.DateTimeField('дата обновления', auto_now=True)

    class Meta:
        abstract = True


class FakeDeleteMixin(models.Model):
    is_deleted = models.BooleanField('удалено', default=False)
    deleted_at = models.DateTimeField('дата удаления', null=True, blank=True)

    class Meta:
        abstract = True
