from django.conf import settings
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import TemplateView

from users.serializers import UserSerializer


class SPAView(TemplateView):
    template_name = 'spa.html'

    def get_current_user(self):
        if self.request.user.is_authenticated:
            user = self.request.user
            user_data = UserSerializer(user).data
            # user_data['logout_url'] = reverse('logout')
            user_data['permissions'] = {
                'can_create_wishlists': 1 if user.has_perm('Wishlist.can_create_wishlists') else 0,
            }
            return user_data
        return {}

    def get_initial_state(self):
        return {}

    def get_context_data(self, **kwargs):
        initial_state = {
            'currentUser': self.get_current_user(),
        }
        addit_init_state = self.get_initial_state()
        if addit_init_state:
            initial_state.update(addit_init_state)

        return {
            'initial_state': initial_state,
            'release': settings.VERSION,
        }


def login(request):
    return render(request, 'login.html')
