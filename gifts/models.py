from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models

from utils.models import TimeStampModelMixin, FakeDeleteMixin
from wishlist.models import Wishlist


class Gift(TimeStampModelMixin, FakeDeleteMixin):
    title = models.CharField('название', max_length=255)
    wishlist = models.ForeignKey(
        to=Wishlist,
        verbose_name='Вишлист',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='wishlist_gifts',
    )
    price_range = models.CharField('диапазон цены', max_length=100, null=True, blank=True)
    link = models.URLField('ссылка', null=True, blank=True)
    image = models.FileField('картинка', null=True, blank=True)
    sorting_key = models.IntegerField('ключ сортировки', default=0)
    is_received = models.BooleanField('получено', default=False)
    comment = models.TextField('комментарий', null=True, blank=True)

    class Meta:
        verbose_name = 'подарок'
        verbose_name_plural = 'подарки'
        ordering = ('sorting_key',)
        unique_together = ('wishlist', 'title')

    def __str__(self):
        return self.title


class Gifter(TimeStampModelMixin):
    gift = models.ForeignKey(
        to=Gift,
        verbose_name='подарок',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='gift_gifters'
    )
    person = models.ForeignKey(
        to=settings.AUTH_USER_MODEL,
        verbose_name='пользователь',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='user_gifters'
    )
    is_primary = models.BooleanField('первичный', default=True)
    is_ready_to_share = models.BooleanField('готов разделить', default=False)

    class Meta:
        verbose_name = 'даритель'
        verbose_name_plural = 'дарители'
        unique_together = ('gift', 'person')

    def __str__(self):
        return f'{self.gift} {self.person}'
