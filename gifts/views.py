from rest_framework.decorators import api_view
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework import generics

from gifts.models import Gift, Gifter
from gifts.serializers import GiftSerializer, GiftCreateSerializer

from wishlist.models import Wishlist


class GiftListView(generics.ListAPIView):
    serializer_class = GiftSerializer

    def get_queryset(self):
        return Gift.objects.filter(wishlist__slug=self.kwargs.get('wishlist_slug'))


class GiftCreateView(generics.CreateAPIView):
    serializer_class = GiftCreateSerializer

    def get_object(self):
        return get_object_or_404(
            Wishlist.objects.filter(person=self.request.user),
            slug=self.kwargs.get('wishlist_slug')
        )

    def post(self, request, *args, **kwargs):
        wishlist = self.get_object()
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(data=self.request.data, context={'wishlist': wishlist})
        if not serializer.is_valid():
            return Response(serializer.errors, status=400)
        gift = serializer.save()
        return Response({'data': GiftSerializer(gift).data})

    def get_serializer_context(self):
        context = super(GiftCreateView, self).get_serializer_context()
        context.update({
            'wishlist': self.request.user,
        })
        return context


class GiftDeleteView(generics.DestroyAPIView):
    lookup_url_kwarg = 'gift_id'
    queryset = Gift.objects.all()

    def get_object(self):
        return get_object_or_404(Wishlist.objects.filter(person=self.request.user), id=self.kwargs.get('gift_id'))


@api_view(['POST'])
def book_gift(request):
    gift = get_object_or_404(Gift, id=request.data.get('gift'))
    if gift.gift_gifters.exists():
        if gift.gift_gifters.first().person.id == request.user.id:
            return Response({
                'msg': 'You cannot give the same gift twice. Although you can try'
            }, status=400)
        return Response({
            'msg': 'This gift is already occupied',
            'gifts': GiftSerializer(gift.wishlist_gifts.all(), many=True).data,
        }, status=400)
    Gifter.objects.create(
        gift=gift,
        person=request.user
    )
    return Response({})


@api_view(['POST'])
def unbook_gift(request):
    gifter = get_object_or_404(Gifter, gift_id=request.data.get('gift'), person=request.user)
    gifter.delete()
    return Response({})
