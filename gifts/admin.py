from django.contrib import admin
from gifts.models import Gift, Gifter


class GiftAdmin(admin.ModelAdmin):
    raw_id_fields = ('wishlist',)
    list_display = ('wishlist', 'title', 'created_at',)
    list_select_related = ('wishlist',)


class GifterAdmin(admin.ModelAdmin):
    raw_id_fields = ('gift', 'person',)
    list_display = ('gift', 'person', 'created_at', 'is_primary')
    list_select_related = ('person', 'gift')


admin.site.register(Gift, GiftAdmin)
admin.site.register(Gifter, GifterAdmin)
