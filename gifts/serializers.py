from rest_framework import serializers
from gifts.models import Gift, Gifter
from wishlist.models import Wishlist


class GiftSerializer(serializers.ModelSerializer):
    gifter = serializers.SerializerMethodField()

    def get_gifter(self, obj):
        return obj.gift_gifters.first().person.id if obj.gift_gifters.exists() and obj.gift_gifters.first().person else None

    class Meta:
        model = Gift
        fields = (
            'id', 'title', 'wishlist', 'price_range', 'link', 'image', 'sorting_key', 'is_received', 'created_at',
            'comment', 'gifter'
        )


class GiftCreateSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        validated_data['wishlist'] = self.context['wishlist']
        wishlist = super(GiftCreateSerializer, self).create(validated_data)
        return wishlist

    class Meta:
        model = Gift
        fields = ('id', 'title', 'price_range', 'link', 'image', 'comment')