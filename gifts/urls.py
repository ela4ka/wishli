from django.urls import path, include
from gifts.views import GiftListView, GiftCreateView, GiftDeleteView, book_gift, unbook_gift


urlpatterns = [
    path('api/', include([
        path('<str:wishlist_slug>/gifts/', GiftListView.as_view(), name='gift_list'),
        path('<str:wishlist_slug>/create/', GiftCreateView.as_view(), name='gift_create'),
        path('<int:gift_id>/delete/', GiftDeleteView.as_view(), name='gift_delete'),
        path('gift/book/', book_gift, name='book_gift'),
        path('gift/unbook/', unbook_gift, name='unbook_gift'),
    ])),
]
