from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    GENDER_MALE = 0
    GENDER_FEMALE = 1

    GENDER_CHOICES = (
        (GENDER_MALE, 'Мужской'),
        (GENDER_FEMALE, 'Женский'),
    )

    username = models.CharField('имя пользователя', max_length=30, unique=True)
    email = models.EmailField('email', max_length=150, blank=True, null=True)
    first_name = models.CharField('имя', max_length=150, blank=True)
    last_name = models.CharField('фамилия', max_length=150, blank=True)
    gender = models.SmallIntegerField('пол', choices=GENDER_CHOICES, blank=True, null=True)

    class Meta(AbstractUser.Meta):
        verbose_name = 'пользователь'
        verbose_name_plural = 'пользователи'

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self, is_name_first=True):
        if not (self.first_name and self.last_name):
            return self.username
        if is_name_first:
            return f'{self.first_name} {self.last_name}'
        return f'{self.last_name} {self.first_name}'

    @property
    def is_male(self):
        return self.gender == User.GENDER_MALE
