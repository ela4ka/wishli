from rest_framework.generics import get_object_or_404

from utils.views import SPAView
from wishlist.models import Wishlist
from wishlist.serializers import WishlistSerializer, WishlistCreateSerializer
from rest_framework import generics


class MainView(SPAView):
    pass


class WishlistDetailView(generics.RetrieveAPIView):
    serializer_class = WishlistSerializer

    def get_object(self):
        return get_object_or_404(Wishlist, slug=self.kwargs.get('wishlist_slug'))


class WishlistCreateView(generics.CreateAPIView):
    queryset = Wishlist.objects.all()
    serializer_class = WishlistCreateSerializer

    def get_serializer_context(self):
        context = super(WishlistCreateView, self).get_serializer_context()
        context.update({
            'person': self.request.user,
        })
        return context
