from django.contrib import admin
from wishlist.models import Wishlist


class WishlistAdmin(admin.ModelAdmin):
    raw_id_fields = ('person',)
    list_display = ('title', 'person', 'occasion', 'created_at')
    list_select_related = ('person',)


admin.site.register(Wishlist, WishlistAdmin)
