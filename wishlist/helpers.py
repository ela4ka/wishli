import random
import string

from django.apps import apps


def create_slug(size=6, max_attempts=5):
    Wishlist = apps.get_model('wishlist', 'Wishlist')
    for _ in range(max_attempts):
        slug = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(size))
        if Wishlist.objects.filter(slug=slug).exists():
            continue
        return slug
    raise ValueError('no slug found')
