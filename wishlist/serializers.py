from rest_framework import serializers

from wishlist.helpers import create_slug
from wishlist.models import Wishlist


class WishlistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wishlist
        fields = ('id', 'title', 'person', 'target_date', 'occasion', 'created_at', 'slug')


class WishlistCreateSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        validated_data['person'] = self.context['person']
        validated_data['slug'] = create_slug()
        wishlist = super(WishlistCreateSerializer, self).create(validated_data)
        return wishlist

    class Meta:
        model = Wishlist
        fields = ('id', 'title', 'occasion', 'person', 'slug')
