from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models

from utils.models import TimeStampModelMixin, FakeDeleteMixin


class Wishlist(TimeStampModelMixin, FakeDeleteMixin):
    title = models.CharField('название', max_length=255)
    slug = models.SlugField('слаг', blank=True, null=True)
    person = models.ForeignKey(
        to=settings.AUTH_USER_MODEL,
        verbose_name='пользователь',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='user_wishlists'
    )
    target_date = models.DateField('дата события', null=True, blank=True)
    expire_date = models.DateField('дата окончания срока', null=True, blank=True)
    occasion = models.CharField('событие', max_length=100, blank=True, null=True)
    image = models.FileField('картинка', null=True, blank=True)
    is_private = models.BooleanField('скрытый', default=False)
    access_code = models.CharField('код доступа', null=True, blank=True, max_length=100)

    class Meta:
        verbose_name = 'Вишлист'
        verbose_name_plural = 'Вишлисты'
        ordering = ('-created_at',)

    def __str__(self):
        return f'{self.title} - {self.person}'
