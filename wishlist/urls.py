from django.urls import path, include
from wishlist.views import WishlistDetailView, WishlistCreateView


urlpatterns = [
    path('api/', include([
        path('<str:wishlist_slug>/detail/', WishlistDetailView.as_view(), name='wishlist_detail'),
        path('create/', WishlistCreateView.as_view(), name='wishlist_create'),
    ])),
]
