import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';
import { fetchApi } from '../../common/helpers';
import { urls } from '../urls';
import './GiftForm.css';

export class GiftForm extends Component {
    state = {
        title: '',
        image: null,
        price: '',
        comment: '',
        link: '',
        titleIsEmpty: false,
    }

    static propTypes = {
        closeDialog: PropTypes.func.isRequired,
        updateGifts: PropTypes.func.isRequired,
        wishlist: PropTypes.string.isRequired,
    }

    onChangeField = (field, value) => {
        this.setState({ [field]: value, titleIsEmpty: false });
    };

    onUploadImage = (event) => {
        this.setState({ image: this.previewImage(event.target.files[0]) });
    };

    previewImage = (file) => {
        return URL.createObjectURL(file);
    };

    submitForm = () => {
        const { title, image, price, comment, link } = this.state;
        if (!title) {
            this.setState({ titleIsEmpty: true });
            return;
        }
        const data = { title, image, link, price_range: price, comment };
        fetchApi(urls.createGift(this.props.wishlist), 'POST', data).then(response => {
            if (response.data) {
                this.props.closeDialog();
                data.id = response.data.id;
                this.props.updateGifts(data);
            }
        });
    };

    getValidationError = () => {
        if (this.state.titleIsEmpty) {
            return 'cannot be empty, sorry';
        }
        return '';
    };

    render() {
        const { image } = this.state;

        return (
            <div
                className="gift-form-base"
            >
                <div className="gift-close-button" onClick={ this.props.closeDialog }>
                    <CloseRoundedIcon className="gift-close-button-icon"/>
                </div>
                <div className="gift-form-container">
                    <div className="gift-input-inner-container">
                        <div className="gift-main-inputs">
                            <div>
                                <div>
                                    <div className="gift-input-title">Title  { this.getValidationError() }</div>
                                    <div className="gift-input-container">
                                        <input
                                            className="gift-form-input short"
                                            type="text"
                                            onChange={
                                                (event) => this.onChangeField('title', event.target.value)
                                            }
                                        />
                                    </div>
                                </div>
                                <div>
                                    <div className="gift-input-title">Max price</div>
                                    <div className="gift-input-container">
                                        <input
                                            className="gift-form-input short"
                                            type="text"
                                            onChange={
                                                (event) => this.onChangeField('price', event.target.value)
                                            }
                                        />
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="gift-input-title separate">Image</div>
                                <div className="gift-input-image">
                                    <img
                                        src={ image ? image : '/static/images/gift.png' }
                                        className="gift-image-default"
                                    />
                                    <div className="gift-image-upload">
                                        <label>
                                            <input
                                                type="file"
                                                id="img"
                                                name="img"
                                                accept="image/*"
                                                onChange={ this.onUploadImage }
                                            />
                                            upload image
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="gift-input-title">Link</div>
                        <div className="gift-input-container">
                            <input
                                className="gift-form-input"
                                type="text"
                                onChange={
                                    (event) => this.onChangeField('link', event.target.value)
                                }
                            />
                        </div>
                        <div className="gift-input-title">Comment</div>
                        <div className="gift-input-container">
                            <input
                                className="gift-form-input"
                                type="text"
                                onChange={
                                    (event) => this.onChangeField('comment', event.target.value)
                                }
                            />
                        </div>
                        <div
                            className="gift-submit-button"
                            onClick={ this.submitForm }
                        >
                            Create gift
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
