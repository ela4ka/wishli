import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';
import { fetchApi } from '../../common/helpers';
import { urls } from '../urls';
import './GiftForm.css';
import './DeleteGiftDialog.css';

export class DeleteGiftDialog extends Component {
    static propTypes = {
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        closeDialog: PropTypes.func.isRequired,
        deleteGift: PropTypes.func.isRequired,
    }

    deleteGift = () => {
        const { id } = this.props;
        fetchApi(urls.deleteGift(id), 'DELETE').then(response => {
            if (response && response.status === 204) {
                this.props.closeDialog();
                this.props.deleteGift(id);
            }
        });
    };

    render() {
        const { title } = this.props;

        return (
            <div
                className="gift-form-base"
            >
                <div className="gift-close-button" onClick={ this.props.closeDialog }>
                    <CloseRoundedIcon className="gift-close-button-icon"/>
                </div>
                <div className="gift-form-container gift-delete-form-container">
                    <div className="gift-input-inner-container">
                        <div className="gift-main-inputs gift-delete-main">
                            { `Are you sure you want to delete "${title}" from your wishlist?` }
                        </div>
                        <div
                            className="gift-submit-button gift-submit-delete-button"
                            onClick={ this.deleteGift }
                        >
                            Delete gift
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
