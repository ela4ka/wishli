import React, { Component } from 'react';
import { DeleteGiftDialog } from './DeleteGiftDialog'
import './Gift.css';

const MAX_LENGTH = 45;

export class Gift extends Component {
    state = {
        commentOpen: null,
    }

    static defaultProps = {
        user: {},
        gift: {},
    }

    getActionButton = (gift) => {
        if (!gift.gifter) {
            return (
                <div
                    className="gift-button"
                    onClick={ () => this.props.bookGift(gift.id) }
                >
                    I take this
                </div>
            )
        }
        if (gift.gifter === this.props.user.id) {
            return (
                <div
                    className="gift-button unbook-button"
                    onClick={ () => this.props.unbookGift(gift.id) }
                >
                    Change the gift
                </div>
            );
        }
        return (
            <div className="gift-button booked-gift-button">
                Occupied
            </div>
        );
    }

    openDeleteDialog = () => {
        this.setState({ isDeleteDialogOpen: true });
    }

    closeDeleteDialog = () => {
        this.setState({ isDeleteDialogOpen: false });
    }

    render() {
        const { commentOpen, isDeleteDialogOpen } = this.state;
        const { user, gift, author } = this.props;

        return (
            <>
                <div className="gift-container" key={ gift.id }>
                    <div className="gift-line" />
                    <div className="gift">
                        { gift.gifter === user.id && <div className="my-gift">Gift from you</div> }
                        <div
                            title={ gift.title }
                            className="gift-title"
                        >
                            { gift.title.slice(0, MAX_LENGTH) }
                            { gift.title.length > MAX_LENGTH && '...' }
                        </div>
                        {
                            user && author === user.id && (
                                <div className="gift-delete-button" onClick={ this.openDeleteDialog }>
                                    delete
                                </div>
                            )
                        }
                        <div className="gift-image-root-container">
                            <div className="gift-image-container">
                                <div className="gift-image-container-rectangle" />
                                <img
                                    alt="Image"
                                    className="gift-image"
                                    src={ gift.image ? gift.image : '/static/images/gift.png' }
                                />
                            </div>
                        </div>
                        <div className="gift-action-container">
                            <div className="gift-view-actions">
                                {
                                    gift.link && (
                                        <div className="gift-link">
                                            <a className="gift-link-href"
                                               href={ gift.link }
                                               target="_blank"
                                            >
                                                <img
                                                    alt="link"
                                                    className="gift-icon"
                                                    src="/static/images/link.svg"
                                                />
                                                link
                                            </a>
                                        </div>
                                    )
                                }
                                {
                                    gift.price_range && (
                                        <div className="gift-price">
                                            <img
                                                alt="Price"
                                                title="Price range"
                                                className="gift-icon-price"
                                                src="/static/images/price.svg"
                                            />
                                            { gift.price_range }
                                        </div>
                                    )
                                }
                                {
                                    gift.comment && (
                                        <div className="gift-comment">
                                            <img
                                                alt="Comment"
                                                className="gift-icon"
                                                src="/static/images/description.svg"
                                                onMouseOver={
                                                    () => { this.setState({ commentOpen: gift.id }) }
                                                }
                                                onMouseOut={
                                                    () => { this.setState({ commentOpen: false }) }
                                                }
                                            />
                                            {
                                                commentOpen === gift.id && (
                                                    <div className="gift-comment-popover">
                                                        { gift.comment }
                                                    </div>
                                                )
                                            }
                                        </div>
                                    )
                                }
                            </div>
                            <div className="gift-book-actions">
                                { this.getActionButton(gift) }
                            </div>
                        </div>
                    </div>
                </div>
                {
                    isDeleteDialogOpen && (
                        <DeleteGiftDialog
                            id={ gift.id }
                            title={ gift.title }
                            closeDialog={ this.closeDeleteDialog }
                            deleteGift={ this.props.deleteGift }
                        />
                    )
                }
            </>
        );
    }
}
