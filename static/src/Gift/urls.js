export const urls = {
    giftList: slug => `/gifts/api/${slug}/gifts/`,
    createGift: slug => `/gifts/api/${slug}/create/`,
    bookGift: '/gifts/api/gift/book/',
    unbookGift: '/gifts/api/gift/unbook/',
    deleteGift: id => `/gifts/api/${id}/delete/`,
};
