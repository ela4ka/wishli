import React, { Component } from 'react';
import './CopyButton.css';

export class CopyButton extends Component {
    state = {
        hintIsShown: false,
    }

    copyLink = () => {
        navigator.clipboard.writeText(window.location);
        this.setState({ hintIsShown: true });
        setTimeout(() => { this.setState({ hintIsShown: false }) }, 2000);
    };

    render() {
        const { hintIsShown } = this.state;
        return (
            <div className="wishlist-copy-container">
                <img src='/static/images/magic.png' className="wishlist-copy-icon"/>
                <span className="wishlist-copy-text" onClick={ this.copyLink }>Copy wishlist link</span>
                { hintIsShown && <span className="wishlist-copy-hint">link is copied!</span> }
            </div>
        );
    }
}
