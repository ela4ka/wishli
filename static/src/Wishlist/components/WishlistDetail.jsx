import React, { Component } from 'react';
import { fetchApi } from '../../common/helpers';
import { urls } from '../urls';
import { urls as giftUrls } from '../../Gift/urls';
import { Gift } from '../../Gift/components/Gift';
import { GiftForm } from '../../Gift/components/GiftForm';
import { CopyButton } from './CopyButton';
import './Wishlist.css';


export class WishlistDetail extends Component {
    state = {
        gifts: [],
        inProgress: true,
        isDialogOpen: false,
        wishlist: {
            id: null,
            title: '',
            occasion: null,
            target_date: null,
            person: null,
        },
    }

    static defaultProps = {
        user: {},
    }

    componentDidMount() {
        fetchApi(giftUrls.giftList(this.props.match.params.slug)).then(data => {
            this.setState({ gifts: data, inProgress: false });
        });
        fetchApi(urls.wishListDetail(this.props.match.params.slug)).then(data => {
            this.setState({ wishlist: data });
        });
    }

    bookGift = (giftId) => {
        if (!this.props.user.id) {
            window.location = '/login/';
            return;
        }
        fetchApi(giftUrls.bookGift, 'POST', { gift: giftId })
            .then(response => {
                let gifts = this.state.gifts.slice();
                const currentGiftId = gifts.findIndex(g => g.id === giftId);
                gifts[currentGiftId].gifter = this.props.user.id;
                this.setState({ gifts });
            });
    }

    unbookGift = (giftId) => {
        fetchApi(giftUrls.unbookGift, 'POST', { gift: giftId })
            .then(response => {
                let gifts = this.state.gifts.slice();
                const currentGiftId = gifts.findIndex(g => g.id === giftId);
                gifts[currentGiftId].gifter = null;
                this.setState({ gifts });
            });
    }

    openDialog = () => {
        this.setState({ isDialogOpen: true });
    };

    closeDialog = () => {
        this.setState({ isDialogOpen: false });
    };

    updateGifts = (data) => {
        const gifts = this.state.gifts.slice();
        gifts.push(data);
        this.setState({ gifts });
    };

    deleteGift = (id) => {
        const gifts = this.state.gifts.filter(gift => gift.id !== id);
        this.setState({ gifts });
    };

    render() {
        const { gifts, isDialogOpen, wishlist, inProgress } = this.state;
        const emptyList = !inProgress && gifts.length === 0;
        const { user } = this.props;

        return (
            <div>
                <div className={ `wishlist-title ${emptyList ? 'empty-title' : ''}` }>
                    { wishlist.title }
                </div>
                {
                    user.id === wishlist.person && (
                        <div
                            className={ `wishlist-add-button ${emptyList ? 'empty-button' : ''}` }
                            onClick={ this.openDialog }
                        >
                            Add gift
                        </div>
                    )
                }
                { user.id === wishlist.person && !emptyList && <CopyButton/> }
                {
                    emptyList && user.id !== wishlist.person && (
                        <div className="wishlist-empty-title">The list is now empty =(</div>
                    )
                }
                <div className={ `wishlist-container ${emptyList ? 'empty-list' : ''}` }>
                    {
                        gifts.map(gift => {
                            return (
                                <Gift
                                    key={ gift.id }
                                    user={ user }
                                    gift={ gift }
                                    bookGift={ this.bookGift }
                                    unbookGift={ this.unbookGift }
                                    deleteGift={ this.deleteGift }
                                    author={ wishlist.person }
                                />
                            );
                        })
                    }
                </div>
                { isDialogOpen && (
                    <GiftForm
                        closeDialog={ this.closeDialog }
                        updateGifts={ this.updateGifts }
                        wishlist={ this.props.match.params.slug }
                    />
                ) }
            </div>
        );
    }
}
