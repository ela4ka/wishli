import React, { Component } from 'react';
import { Redirect } from 'react-router';
import './MainPage.css';
import '../../Gift/components/GiftForm.css';
import { urls } from '../urls.js';
import { fetchApi } from '../../common/helpers';



export class MainPage extends Component {
    state = {
        occasion: 'Birthday',
        title: '',
        inProgress: false,
        redirectUrl: null,
    }

    onChangeTitle = (event) => {
        this.setState({ title: event.target.value });
    };

    onChangeOccasion = (event) => {
        this.setState({ occasion: event.target.value });
    };

    submitForm = () => {
        this.setState({ inProgress: true});
        const data = {
            occasion: this.state.occasion,
            title: this.state.title,
        }
        fetchApi(urls.submitForm, 'POST', data).then(response => {
            if (response && response.slug) {
                this.setState({ redirectUrl: urls.wishlistDetail(response.slug) });
            }
            this.setState({ inProgress: false });
        });
    };

    render() {
        const { inProgress, redirectUrl } = this.state;

        if (redirectUrl) {
            return <Redirect to={ redirectUrl } />
        }

        return (
            <div className="wishlist-main">
                <div>
                    <div className="gift-main-cover"/>
                    <div className="gift-main-cover-top"/>
                    <div className="gift-main-description">
                        Create a wishlist and share your dreams with friends
                    </div>
                    <img className="gift-description-image" src="/static/images/star.png"/>
                    <div className="gift-main-form-container">
                    <div className="gift-input-inner-container">
                        <div className="gift-input-title">Event title</div>
                        <div className="gift-input-container">
                            <input
                                className="gift-main-form-input"
                                type="text"
                                onChange={ this.onChangeTitle }
                                spellcheck="false"
                            />
                        </div>
                        <div className="gift-input-title">Occasion</div>
                        <div className="gift-input-container">
                            <select className="gift-main-form-input" onChange={ this.onChangeOccasion }>
                                <option value="Birthday">Birthday</option>
                                <option value="Anniversary">Anniversary</option>
                                <option value="Wedding">Wedding</option>
                                <option value="Childbirth">Childbirth</option>
                                <option value="Housewarming">Housewarming</option>
                                <option value="Personal">Personal</option>
                            </select>
                        </div>
                        <div className="wish-button" onClick={ inProgress ? () => {} : this.submitForm }>
                            Make a wish
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}
