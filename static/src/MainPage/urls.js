export const urls = {
    submitForm: '/lists/api/create/',
    wishlistDetail: slug => `/list/${slug}/`,
}