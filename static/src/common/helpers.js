import {urls} from "../Wishlist/urls";

export function transformToCamel(value) {
    return value.replace(/[a-zA-Z0-9]_[a-zA-Z0-9]/g, m => `${m[0]}${m[2].toUpperCase()}`);
}

export function objCamelFromSnake(obj) {
    if (!obj) {
        return obj;
    }
    const keyToCamel = key => transformToCamel(key);
    let newObj;
    let origKey;
    let newKey;
    let value;
    if (obj instanceof Array) {
        newObj = [];
        for (origKey = 0; origKey < obj.length; origKey++) {
            value = obj[origKey];
            if (typeof value === 'object') {
                value = objCamelFromSnake(value);
            }
            newObj.push(value);
        }
    } else {
        newObj = {};
        for (origKey in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, origKey)) {
                newKey = keyToCamel(origKey);
                value = obj[origKey];
                if (value instanceof Array || (value !== null && value !== undefined && value.constructor === Object)) {
                    value = objCamelFromSnake(value);
                }
                newObj[newKey] = value;
            }
        }
    }
    return newObj;
}

export function objSnakeFromCamel(obj) {
    if (!obj) {
        return obj;
    }
    let newObj;
    if (obj instanceof Array) {
        newObj = obj.map((value) => {
            if (typeof value === 'object') {
                return objSnakeFromCamel(value);
            }
            return value;
        });
    } else {
        newObj = {};
        Object.keys(obj).forEach((key) => {
            const newKey = key.replace(/\.?([A-Z]+)/g, (match, group) => `_${group.toLowerCase()}`).replace(/^_/, '');
            let value = obj[key];
            if (value instanceof Array || (value !== null && value !== undefined && value.constructor === Object)) {
                value = objSnakeFromCamel(value);
            }
            newObj[newKey] = value;
        });
    }
    return newObj;
}

/* eslint-disable prefer-rest-params */

// простая проверка, что объект содержит пустые ключи
export function isEmptyObject(obj) {
    if (typeof obj !== 'object') {
        throw new Error(`аргумент isEmptyObject ${obj} не является объектом`);
    }
    if (Array.isArray(obj)) {
        throw new Error('аргумент isEmptyObject является массивом');
    }
    for (const key in obj) {
        if (obj[key] !== null && obj[key] !== '' && obj[key] !== undefined) {
            return false;
        }
    }
    return true;
}

// склонение существительных
// declOfNum(сount, ['получатель', 'получателя', 'получателей']);
export function declOfNum(number, titles) {
    const absNumber = Math.abs(number);
    const cases = [2, 0, 1, 1, 1, 2];
    return titles[(absNumber % 100 > 4 && absNumber % 100 < 20) ? 2 : cases[(absNumber % 10 < 5) ? absNumber % 10 : 5]];
}

export function isObject(item) {
    return (typeof item === 'object' && !Array.isArray(item) && item !== null);
}

export function debounce(originalFn, timeout = 100) {
    let timer = null;
    return function debounced(...args) {
        function delayed() {
            timer = null;
            originalFn.apply(this, args);
        }

        if (timer !== null) {
            clearTimeout(timer);
        }
        timer = setTimeout(delayed, timeout);
    };
}

function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
}

export function fetchApi(url, method= 'GET', body = {}) {
    const params = {
        method: method,
        headers: { 'Content-Type': 'application/json'},
        credentials: 'same-origin',
    }
    if (method !== 'GET') {
        params.body = JSON.stringify(body)
        params.headers['X-CSRFToken'] = getCookie('csrftoken');
    }
    return fetch(url, params).then(response => {
        if (response.status > 400) {
            console.log(response);
            return;
        }
        if (response.status === 204) {
            return { status: 204 };
        } else {
            return response.json();
        }
    });
}
