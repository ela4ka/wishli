import {
    createStore,
    applyMiddleware,
    combineReducers,
    compose,
} from 'redux';
import apiMiddleware from '../common/middlewares/apiMiddleware';
// import currentUserReducer from '../common/currentUser/reducers';

// const staticReducers = {
//     currentUser: currentUserReducer,
// };

const preloadedStateStore = window.initial_state;
function prepare(storeData) {
    // const newStore = storeData ? { ...storeData } : { currentUser: { permissions: {} } };
    // let newPerms = {};
    // if (storeData && storeData.currentUser) {
    //     newPerms = {
    //         canCreateWishlists: storeData.currentUser.permissions
    //             && storeData.currentUser.permissions.can_create_wishlists === 1,
    //     };
    // }
    // newStore.currentUser.permissions = newPerms;
    // return newStore;
    return storeData;
}

function reducerWithPreloadedState(key, reducer, preloadedState = {}) {
    if (preloadedState[key]) {
        return function (state = preloadedState[key], action) {
            return reducer(state, action);
        };
    }
    return reducer;
}

function createReducer(asyncReducers, preloadedState) {
    // const reducers = { ...staticReducers, ...asyncReducers };
    const reducers = { ...asyncReducers };
    const reducersWithPreloadedState = Object.keys(reducers).reduce((prevState, key) => ({
        ...prevState,
        [key]: reducerWithPreloadedState(key, reducers[key], preloadedState),
    }), {});
    return combineReducers(reducersWithPreloadedState);
}

function configureStore() {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    const preloaded = prepare(preloadedStateStore);

    const store = createStore(
        createReducer({}, preloaded),
        composeEnhancers(applyMiddleware(apiMiddleware)),
    );

    store.asyncReducers = {};

    store.injectReducer = (key, asyncReducer) => {
        store.asyncReducers[key] = asyncReducer;
        store.replaceReducer(createReducer(store.asyncReducers, preloaded));
    };

    return store;
}

const spaStore = configureStore();

export default spaStore;
