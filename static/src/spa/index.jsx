import 'core-js/stable';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import Router from './router';
import spaStore from './store';

// addLocaleData(ruLocaleData);

const rootEl = document.getElementById('spa_container');
const initialData = window.initial_state

ReactDOM.render(
    <Provider store={ spaStore }>
        <div id="main-content" className='main-content'>
            <Router initial={ initialData }/>
        </div>
    </Provider>,
    rootEl,
);
