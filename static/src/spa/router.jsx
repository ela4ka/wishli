import React, { Component, Suspense } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { MainPage } from '../MainPage/components/MainPage';
import { WishlistDetail } from '../Wishlist/components/WishlistDetail';


export default class Router extends Component {
    render() {
        return (
            <BrowserRouter>
                <Suspense>
                    <Switch>
                        <Route
                            path="/"
                            render={ props => <MainPage user={ this.props.initial.currentUser } { ...props } /> }
                            exact
                        />
                        <Route
                            path="/list/:slug/"
                            render={ props => <WishlistDetail user={ this.props.initial.currentUser } { ...props } /> }
                            exact
                        />
                    </Switch>
                </Suspense>
            </BrowserRouter>
        );
    }
}
