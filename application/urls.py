from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from wishlist.views import MainView
from utils import views as utils_views


urlpatterns = [
    path('', MainView.as_view(), name='main_page'),
    path('list/<str:slug>/', MainView.as_view(), name='main_page'),
    path('lists/', include('wishlist.urls')),
    path('gifts/', include('gifts.urls')),
    path('admin/', admin.site.urls),
    path('login/', utils_views.login, name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('social_auth/', include('social_django.urls', namespace='social')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
