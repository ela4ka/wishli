import json
import os
import sys
from configparser import RawConfigParser, NoOptionError, NoSectionError
from pathlib import Path


PROJECT_NAME = 'wishli'

BASE_DIR = Path(__file__).resolve().parent.parent
SOURCE_ROOT = (os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

global_config_path = '/etc/{0}/django.conf'.format(PROJECT_NAME)

local_config_path = os.path.join(
    PROJECT_ROOT, 'conf', '{0}.conf'.format(PROJECT_NAME)
)

try:
    build_conf = json.load(open(os.path.join(SOURCE_ROOT, 'deploy', 'BUILD.json')))
    VERSION = ('v' + build_conf['version']).strip()
except:
    VERSION = 'v=1'

config = RawConfigParser()

if os.path.exists(local_config_path):
    config.read(local_config_path, encoding='utf8')
else:
    config.read(global_config_path, encoding='utf8')


def config_get(section, option, default=None, is_bool=False):
    try:
        value = config.get(section, option)
    except (NoSectionError, NoOptionError):
        value = default
    return bool(int(value)) if is_bool else value


SECRET_KEY = config_get('global', 'SECRET_KEY', 'secret')

DEBUG = False

ALLOWED_HOSTS = ['*']

AUTH_USER_MODEL = 'users.User'


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_filters',
    'rest_framework',
    'social_django',
    'webpack_loader',
    'sslserver',
]

PROJECT_APPS = [
    'gifts',
    'users',
    'utils',
    'wishlist',
]

INSTALLED_APPS += PROJECT_APPS

LOGIN_URL = 'login'
LOGIN_REDIRECT_URL = 'main_page'
LOGOUT_URL = 'logout'
LOGOUT_REDIRECT_URL = 'login'

SOCIAL_AUTH_FACEBOOK_KEY = config_get('social', 'SOCIAL_AUTH_FACEBOOK_KEY', 'SOCIAL_AUTH_FACEBOOK_KEY')
SOCIAL_AUTH_FACEBOOK_SECRET = config_get('social', 'SOCIAL_AUTH_FACEBOOK_KEY', 'SOCIAL_AUTH_FACEBOOK_SECRET')
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
  'locale': 'ru_RU',
  'fields': 'id, name, email'
}

AUTHENTICATION_BACKENDS = [
    'social_core.backends.facebook.FacebookOAuth2',
    'django.contrib.auth.backends.ModelBackend',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'application.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(SOURCE_ROOT, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'application.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': config_get('database_default', 'DATABASE_BACKEND', 'django.db.backends.postgresql_psycopg2'),
        'NAME': config_get('database_default', 'DATABASE_NAME'),
        'USER': config_get('database_default', 'DATABASE_USER'),
        'PASSWORD': config_get('database_default', 'DATABASE_PASSWORD'),
        'HOST': config_get('database_default', 'DATABASE_HOST'),
        'PORT': config_get('database_default', 'DATABASE_PORT'),
    }
}

# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        # 'oauth2_provider.contrib.rest_framework.OAuth2Authentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_RENDERER_CLASSES': [
        'rest_framework.renderers.JSONRenderer',
    ]
}


# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'ru-RU'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True

ADMINS = (
    ('Алена Елизарова', 'ela4ka@yandex.ru'),
)


STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')

if 'collectstatic' in sys.argv:
    STATIC_ROOT = os.path.join(SOURCE_ROOT, 'collected_static')

STATICFILES_DIRS = (
    os.path.join(SOURCE_ROOT, 'static'),
)

WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': 'dist/',
        'STATS_FILE': os.path.join(SOURCE_ROOT, 'webpack-configs/webpack-stats.json'),
    }
}

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

DISK_ROOT = (os.path.dirname(os.path.dirname(__file__))) + '/'
MEDIA_ROOT = DISK_ROOT + '../'
MEDIA_URL = '/media/'
DEFAULT_FILE_STORAGE = 'django.core.files.storage.FileSystemStorage'

try:
    # pylint: disable=wildcard-import
    from application.local_settings import *
except ImportError:
    pass
