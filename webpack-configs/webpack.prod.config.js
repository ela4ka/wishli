const webpack = require('webpack');
const merge = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const config = require('./webpack.common');


module.exports = merge(config, {
    mode: 'production',
    optimization: {
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    ie8: true,
                },
            }),
        ],
    },
    output: {
        publicPath: '/static/dist/',
    },
});
