const webpack = require('webpack');
const merge = require('webpack-merge');
const config = require('./webpack.common');

module.exports = merge(config, {
    mode: 'development',
    output: {
        publicPath: 'http://127.0.0.1:3000/',
        sourceMapFilename: '[name].js.map',
        filename: '[name].entry.js'
    },
    devServer: {
        contentBase: '../static/dist',
        host: '127.0.0.1',
        port: '3000',
        headers: { 'Access-Control-Allow-Origin': '*' },
        disableHostCheck: true,
        writeToDisk: true,
    },
    devtool: 'eval-source-map',
});
