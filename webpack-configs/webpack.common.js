const BundleTracker = require('webpack-bundle-tracker');
const path = require('path');

module.exports = {
    entry: {
        spa: './static/src/spa/index.jsx',
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: [
                    /node_modules/,
                    /static\/vendor/,
                ],
                loader: [
                    'babel-loader',
                    'ts-loader',
                ],
            },
            {
                test: /\.(jsx?|es6)$/,
                exclude: [
                    /node_modules/,
                    /static\/vendor/,
                ],
                loaders: [
                    'babel-loader',
                ],
            },
            {
                test: /\.css$/,
                loaders: [
                    'style-loader',
                    'css-loader',
                ],
            },
            {
                test: /\.(png|svg)$/,
                loaders: [
                    'url-loader?limit=100000',
                ],
            },
            {
                test: /\.(woff2?|eot|ttf|otf)/,
                loaders: [
                    'url-loader?limit=100000',
                ],
            },
            {
                test: /\.gif$/,
                loaders: ['file-loader'],
            },
        ],
    },
    resolve: {
        extensions: ['*', '.js', '.jsx'],
        alias: {
            '@': path.resolve(__dirname, '../static/src'),
        },
        modules: [
            path.resolve(__dirname, '../static/'),
            'node_modules',
        ],
    },
    output: {
        path: path.resolve(__dirname, '../static/dist'),
        sourceMapFilename: '[name].js.map',
        filename: '[name].[hash].entry.js',
    },
    plugins: [
        new BundleTracker({
            filename: 'webpack-stats.json',
            path: __dirname,
        }),
    ],
    devtool: 'source-map',
};
